<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
		return view('welcome');
	});
Route::get('/index', 'PostController@index')->name('index');
Route::get('/my-post', 'PostController@show')->name('show');
Route::get('/posts/create', 'PostController@create')->name('post.create');
Route::post('/posts', 'PostController@store')->name('post.store');
Route::get('/posts/edit/{id}', 'PostController@edit')->name('post.edit');
Route::put('/posts/update/{id}', 'PostController@update')->name('post.update');
Route::delete('/posts/delete/{id}', 'PostController@destroy')->name('post.delete');

Route::post('/comment/store', 'CommentController@store')->name('comments.store');
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');

Route::get('/profile/show', 'ProfileController@show')->name('profile.show');
Route::get('/profile/create', 'ProfileController@create')->name('profile.create');
Route::post('/profile/store', 'ProfileController@store')->name('profile.store');
Route::get('/profile/edit/{id}', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile/update/{id}', 'ProfileController@update')->name('profile.update');

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
