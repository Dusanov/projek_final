<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
            <!-- Font Awesome -->
            <link rel="stylesheet" href="{{ asset('/adminLte/plugins/fontawesome-free/css/all.min.css')}}">
            <!-- Ionicons -->
            <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
            <!-- overlayScrollbars -->
            <link rel="stylesheet" href="{{ asset('/adminLte/dist/css/adminlte.min.css')}}">
            <!-- Google Font: Source Sans Pro -->
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
            <style>
                body{
                     background-image: url('{{ asset('adminLte/img/bg.jpg') }}');
                    }
            </style>
        <!-- <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style> -->
    </head>
    <body>
        <!-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>
            </div>
        </div> -->
            <nav class="bg-secondary sticky-top">
         <div class="container">
          <div class="row">
            <div class="col-6 py-1">
              <img src="{{asset ('adminLte/img/logo.png')}}" class="align-self-center mr-3" width="75" height="auto" alt="...">
            </div>
            <div class="col-6 py-2">
                @if (Route::has('login'))
                <ul class="nav justify-content-end">
                @auth
                  <li class="nav-item">
                    <a class="nav-link text-light" href="{{ url('/home') }}">Home</a>
                  </li>
                @else
                  <li class="nav-item">
                    <a class="nav-link text-light" href="{{ route('login') }}">Login</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                        <a class="nav-link text-light" href="{{ route('register') }}">Register</a>
                      </li>
                  @endif
                @endauth
                @endif
                </ul>
            </div>  
          </div>
         </div>
        </nav>
        <div class="container">
            <div class="row mt-5 ml-2">
                <div class="col-5 mr-2">
                        <p class="text-dark h1">Tempat Diskusi dan bertanya Lorem ipsum, utawa ringkesé lipsum, iku tèks standar sing dipasang kanggo nuduhaké èlemèn grafis utawa presentasi visual kaya font, tipografi, lan tata letak</p>      
                </div>
            </div>       
        </div>    
    </body>
</html>
