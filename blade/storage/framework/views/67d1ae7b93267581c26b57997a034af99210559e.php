<?php $__env->startSection('content'); ?>

  <div class="ml-3 mt-3" style="padding-top: 20px;">
	  <div class="card card-primary">
      <div class="card-header">
        <h4 class="card-title">Tanya Coding</h4>
      </div>
            <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody>
              <tr>
                <th>ID</th>
                <th>Nama User</th>
                <th>Question</th>
                <th>Button</th>
                <th>Komentar</th>
              </tr>
              <tr>
                <td>183</td>
                <td>John Doe</td>
                <td>Bagaimana Install Laravel?</td>
                <td><button type="submit" class="btn btn-primary">Create</button>
                <button type="submit" class="btn btn-primary">Edit</button>
                <button type="submit" class="btn btn-primary">Show</button></td>
                <td><button type="submit" class="btn btn-primary">Comment</button></td>
              </tr>
              
          </tbody>
        </table>
      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layoutsAdminlte.masterTemplate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\FinalProject\blade\resources\views/items/index.blade.php ENDPATH**/ ?>