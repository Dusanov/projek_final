<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="{{ asset('adminlte/img/logo.png')}}"
         alt="Q&A"
         class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Q&A</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      @guest
      @if (Route::has('login'))
      <div class="info">
        <a href="{{route ('login')}}" class="d-block">{{ __('Anda belum login') }}</a>
      </div>
      @endif
      @else
      <div class="image">@isset($profile)
        <img src="{{ asset('images/'.$profile->photo)}}" class="img-circle elevation-2" alt="User Image">
      @endisset
      </div>
      <div class="info">
        <a href="{{route ('profile.show')}}" class="d-block">{{ Auth::user()->name }}</a>
      </div>
      @endguest
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
          <a href="{{ route('profile.show') }}" class="nav-link">
            <i class="nav-icon fas fa-user"-alt></i>
            <p>
              Profile
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ route('show') }}" class="nav-link">
            <i class="nav-icon far fa-clipboard"></i>
            <p>
              My Post
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ route('index') }}" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>