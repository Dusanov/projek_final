@extends('layouts.app')

@section('content')                   
<div class="container-fluid">
  
  <div class="row justify-content-center">
   
   
  <div class="col-md-6 ">
    <a href="{{route('profile.create')}}" class="btn btn-info mb-4"><i class="fas fa-cog"></i> Isi Profile </a>
    @foreach ($profiles as $profile)
    
    @if (auth()->user()->is($profile->user))
    <div class="card mb-4">
      <div class="card-header bg-light">
        <p>Photo</p>
        <img src="{{ asset('images/'.$profile->photo )}}" class="rounded card-img-top"  >  
      </div>
      <div class="card-body">
        <div>
          <p>Full Name </p>
          <h3>{{ $profile->full_name }}</h3>
          <hr>
        </div>
        <div>
          <p>Phone Number</p>
          {{ ($profile->phone) }}
        </div>
      </div>
      <a href="{{route('profile.edit', $profile->id)}}" class="btn btn-warning"><i class="fas fa-edit"></i> Edit Profile </a>
    </div>
    @endif
    @endforeach
  </div>
  
   
  </div> 
</div>

@endsection