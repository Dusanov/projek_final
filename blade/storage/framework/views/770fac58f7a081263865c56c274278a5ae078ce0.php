<?php $__env->startSection('content'); ?>

    <div class="ml-3 mt-3" style="padding-top: 20px;"> 
      <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create Post</h3>
        </div>
              <!-- /.card-header -->
              <!-- form start -->
        <form role="form" action="/posts" method="POST">
            <?php echo csrf_field(); ?>
            <div class="card-body">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">
                </div>
                <div class="form-group">
                    <label for="question">Question</label>
                    <input type="text" class="form-control" id="question" name="question" placeholder="Your Question">
                </div>
            </div>
                <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
      </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layoutsAdminlte.masterTemplate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\projek_final\blade\resources\views/posts/create.blade.php ENDPATH**/ ?>