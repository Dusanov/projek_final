@extends('layouts.app')

@section('content')                   
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">Edit profle</div>
        <div class="card-body">
          <form action="{{ route ('profile.update', $profile->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
              <label for="full_name">Full Name</label>
              <input type="text" class="form-control" name="full_name" id="full_name" value="{{ $profile->full_name }}">
              @error('full_name')
                  <div class="text-danger mt-2">
                    {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="phone">Phone Number</label>
              <input type="text" class="form-control" name="phone" id="phone" {{ $profile->phone }}>
              @error('phone')
                  <div class="text-danger mt-2">
                    {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="photo">Photo</label>
              <br>
              <img src="{{ asset('images/'.$profile->photo)}}" style="width: 450px"><br>
              <label for="photo">Ganti Photo</label>
              <input type="file" class="form-control-file" name="photo" id="photo">
              <label>*) Jika Gambar tidak diganti, kosongkan saja</label>
              @error('photo')
              <div class="text-danger mt-2">
                {{ $message }}
              </div>
              @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit profil</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
      
@endsection