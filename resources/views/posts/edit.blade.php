@extends('layouts.app')

@section('content')                   
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">Update Post {{ $post->title }}</div>
        <div class="card-body">
          <form action="{{route('post.update', $post->id)}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" value="{{ $post->title }}" name="title" id="title">
              @error('title')
                  <div class="text-danger mt-2">
                    {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="body">Body</label>
              <textarea type="text" class="form-control"  name="body" id="body">{{ $post->body }}</textarea>
              @error('body')
              <div class="text-danger mt-2">
                {{ $message }}
              </div>
              @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit post</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
       
@endsection