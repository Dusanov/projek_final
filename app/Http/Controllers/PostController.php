<?php

namespace App\Http\Controllers;
use App\Post;

use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']); // or ->only(['']) = untuk mengamankan route
    }

    public function index()
    {
        $posts = Post::all();
        return view('index', compact('posts'));
    }

    public function show()
    {
        $posts = Post::all();
        return view('posts.show', compact('posts'));
    }

    public function create() {

        Alert::toast('Silahkan Tulis', 'Berhasil');
    	return view('posts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' =>'required',
            'body' => 'required',
        ]);

        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = auth()->id();
        $post->save();


        Alert::success('Success Title', 'Berhasil Menambahkan Items');
        return redirect('index');
    }

    public function edit($id)
    {   
        $post = Post::find($id);


        return view('posts.edit', compact('post'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' =>'required',
            'body' => 'required',
        ]);

        $post = Post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = auth()->id();
        $post->update();


        return redirect('index');
    }

    public function destroy($id)
    {   
        $post = Post::find($id);
        $post->delete();
        return redirect('index');
    }
}
