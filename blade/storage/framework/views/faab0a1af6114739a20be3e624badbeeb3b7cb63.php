<?php $__env->startSection('content'); ?>

  <div class="ml-3 mt-3" style="padding-top: 20px;">
	  <div class="card card-primary">
      <div class="box-header">
        <h3 class="box-title">Tanya Coding</h3>
      </div>
            <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody>
              <tr>
                <th>ID</th>
                <th>User</th>
                <th>Question</th>
                <th>Button</th>
                <th>Answer</th>
              </tr>
              <tr>
                <td>183</td>
                <td>John Doe</td>
                <td>Bagaimana Install Laravel?</td>
                <td><span class="label label-success">Approved</span></td>
                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
              </tr>
              
          </tbody>
        </table>
      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layoutsAdminlte.masterTemplate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tugas-adminlte\blade\resources\views/items/index.blade.php ENDPATH**/ ?>