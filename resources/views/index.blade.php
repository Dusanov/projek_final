@extends('layouts.app')
<style>
  #more {display: none;}
</style>

@section('content')                   
<div class="container-fluid">
  <div class="d-flex justify-content-between">
    <div>
      <h4>All Article</h4>
      <hr>
    </div>
    <div>
      <a href="{{ route('post.create')}}" class="btn btn-primary">Add new Article</a>
    </div>
  </div>
  <div class="row justify-content-center">
   
    <div class="col-md-6">
      @foreach ($posts as $post)
      <div class="card mb-4">
        <div class="card-header bg-light">
            <h3>{{ $post->title }}</h3>
            <div class="text-secondary">
              wrote by {{ $post->user->name }}
            </div>
        </div>
        <div class="card-body">
          <div>
            {{ ($post->body) }}
          </div>
          <div>
            <hr>
          </div>
        </div>
        <div class="card-footer">
          <div class="pb-5">
            <button class="btn btn-outline-dark btn-sm" id="myBtn" onclick="myFunction();" >See Comments</button>
          </div>
          <span  id="dots"></span>
          <span id="more">
            @include('posts.commentsDisplay', ['comments' => $post->comments, 'post_id' => $post->id])
            <hr />
            <form method="post" action="{{ route('comments.store'   ) }}">
            @csrf
              <div class="form-group">
                <textarea class="form-control" name="body"></textarea>
                <input type="hidden" name="post_id" value="{{ $post->id }}" />
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-success" value="Add Comment" />
              </div>
            </form>
          </span>
        </div>
      </div>
      @endforeach
    </div>
  </div> 
</div>

@endsection
@push('script')
<script>
  function myFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "See Comments"; 
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Hide Comments"; 
      moreText.style.display = "inline";
    }
  }

</script>

@endpush