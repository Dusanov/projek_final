@extends('layouts.app')

@section('content')                   
<div class="container-fluid">
  
  <div class="row justify-content-center">
  
  <div class="col-md-6">
    @foreach ($posts as $post)
    @if (auth()->user()->is($post->user))
    <div class="card mb-4">
      <div class="card-header bg-light">
          <h3>{{ $post->title }}</h3>
          <div class="text-secondary">
            wrote by {{ $post->user->name }}
          </div>
      </div>
      <div class="card-body">
        <div>
          {{ ($post->body) }}
        </div>
        <div>
          <hr>
        </div>
      </div>
      <div class="card-footer">
        <form action="{{route('post.delete', $post->id)}}" method="POST">
          @csrf
          @method('delete')
          <a href="{{route('post.edit', $post->id)}}" class="btn btn-link text-info p-0 ">Edit</a>
          <button onclick="return confirm('Yakin mau dihapus ?')" type="submit" class="btn btn-link text-danger ml-4">Delete</button>
        </form>
      </div>
    </div>
    @endif
    @endforeach
  </div>
  
   
  </div> 
</div>

@endsection