<?php

namespace App\Http\Controllers;
use App\Profile;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // or ->only(['']) = untuk mengamankan route
    }

    public function show()
    {
        $profiles = Profile::all();
        return view('profile.show', compact('profiles'));
    }


    public function create() {

    	return view('profile.create');
    }
    

    public function store (Request $request)
    {
        $this->validate($request, [
            'full_name' =>'required',
            'phone' => 'required',
            'photo' => 'required|image|mimes:jpeg,jpg,png',
        ]);

        $profile = New Profile;
        $profile->full_name = $request->full_name;
        $profile->phone = $request->phone;
        $profile->user_id = auth()->id();

        $photo = $request ->photo;
        $filename = time().'.'.$photo->getClientOriginalExtension();
        $photo->move('images/', $filename);

        $profile->photo = $filename;
        $profile->save();
        
        Alert::success('Success Title', 'Berhasil Edit Profile');
        return redirect('index');
    }

    public function edit($id){
        $profile = Profile::find($id);
        return view('profile.edit', compact('profile'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'full_name' =>'required',
            'phone' => 'required',
            'photo' => 'required|image|mimes:jpeg,jpg,png',
        ]);

        $profile = Profile::find($id);
        if ($request->has('photo')){
            $profile->full_name = $request->full_name;
            $profile->phone = $request->phone;
                
            $photo = $request ->photo;
            $filename = time().'.'.$photo->getClientOriginalExtension();
            $photo->move('images/', $filename);

    
            $profile->photo = $filename;
        }
        else{
            $profile->full_name = $request->full_name;
            $profile->phone = $request->phone;
        }

        $profile->update();
        return redirect('profile/show');
    }

}
